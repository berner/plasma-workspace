# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2019, 2020, 2021.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022, 2023.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-09 02:03+0000\n"
"PO-Revision-Date: 2023-04-07 23:30-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.12.3\n"

#: kcm.cpp:71
#, kde-format
msgid "Toggle do not disturb"
msgstr "マナーモードを切り替え"

#: sourcesmodel.cpp:391
#, kde-format
msgid "Other Applications"
msgstr "その他のアプリケーション"

#: ui/ApplicationConfiguration.qml:91
#, kde-format
msgid "Show popups"
msgstr "ポップアップを表示"

#: ui/ApplicationConfiguration.qml:105
#, kde-format
msgid "Show in do not disturb mode"
msgstr "マナーモードでも表示"

#: ui/ApplicationConfiguration.qml:118 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "履歴に表示"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid "Show notification badges"
msgstr "通知バッジを表示"

#: ui/ApplicationConfiguration.qml:178
#, fuzzy, kde-format
#| msgid "Configure Events…"
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "イベントを設定..."

#: ui/ApplicationConfiguration.qml:204
#, fuzzy, kde-format
#| msgid ""
#| "This application does not support configuring notifications on a per-"
#| "event basis."
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr "このアプリケーションはイベントごとの通知の設定をサポートしていません。"

#: ui/ApplicationConfiguration.qml:291
#, kde-format
msgid "Show a message in a pop-up"
msgstr ""

#: ui/ApplicationConfiguration.qml:300
#, kde-format
msgid "Play a sound"
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"通知ウィジェットが見つかりませんでした。通知を表示するには通知ウィジェットが"
"必要です。システムトレイにて、もしくはスタンドアローンのウィジェットとして有"
"効化されていることを確認してください。"

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "現在、通知は Plasma の代わりに '%1 %2' によって提供されています。"

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "現在、通知は Plasma によって提供されていません。"

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "マナーモード"

#: ui/main.qml:68
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "Enable:"
msgstr "有効:"

#: ui/main.qml:69
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "画面がミラーリングされているとき"

#: ui/main.qml:81
#, kde-format
msgctxt "Enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "スクリーン共有中"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Keyboard shortcut:"
msgstr "キーボードショートカット:"

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "可視性"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "重大な通知:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "マナーモードでも表示"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "通常の通知:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "フルスクリーンウィンドウの上に表示"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "優先度の低い通知:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "ポップアップを表示"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "ポップアップ"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "場所:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "通知アイコンの近く"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "表示位置を指定…"

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 秒"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "非表示にするまでの時間:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "追加のフィードバック"

#: ui/main.qml:250
#, kde-format
msgid "Application progress:"
msgstr "アプリケーションの進行状況:"

#: ui/main.qml:251 ui/main.qml:291
#, kde-format
msgid "Show in task manager"
msgstr "タスクマネージャに表示"

#: ui/main.qml:263
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "通知に表示"

#: ui/main.qml:277
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "進行中はポップアップを開けたままにする"

#: ui/main.qml:290
#, kde-format
msgid "Notification badges:"
msgstr "通知バッジ:"

#: ui/main.qml:302
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "アプリケーション固有の設定"

#: ui/main.qml:307
#, kde-format
msgid "Configure…"
msgstr "設定…"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "ポップアップの位置"

#: ui/SourcesPage.qml:19
#, kde-format
msgid "Application Settings"
msgstr "アプリケーションの設定"

#: ui/SourcesPage.qml:99
#, kde-format
msgid "Applications"
msgstr "アプリケーション"

#: ui/SourcesPage.qml:100
#, kde-format
msgid "System Services"
msgstr "システムサービス"

#: ui/SourcesPage.qml:138
#, fuzzy, kde-format
#| msgid "No application or event matches your search term."
msgid "No application or event matches your search term"
msgstr "検索語に一致するアプリケーションおよびイベントはありません。"

#: ui/SourcesPage.qml:161
#, fuzzy, kde-format
#| msgid ""
#| "Select an application from the list to configure its notification "
#| "settings and behavior."
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr "リストからアプリケーションを選択して通知設定と動作を設定します。"

#~ msgid "Configure Notifications"
#~ msgstr "通知を設定"
