# Translation of plasma_engine_soliddevice to Norwegian Nynorsk
#
# Eirik U. Birkeland <eirbir@gmail.com>, 2010.
# Karl Ove Hufthammer <karl@huftis.org>, 2010, 2015, 2018, 2019, 2020.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-19 01:58+0000\n"
"PO-Revision-Date: 2020-11-08 15:13+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: soliddeviceengine.cpp:113
msgid "Parent UDI"
msgstr "Forelder-UDI"

#: soliddeviceengine.cpp:114
msgid "Vendor"
msgstr "Produsent"

#: soliddeviceengine.cpp:115
msgid "Product"
msgstr "Produkt"

#: soliddeviceengine.cpp:116
msgid "Description"
msgstr "Skildring"

#: soliddeviceengine.cpp:117
msgid "Icon"
msgstr "Ikon"

#: soliddeviceengine.cpp:118 soliddeviceengine.cpp:593
msgid "Emblems"
msgstr "Emblem"

#: soliddeviceengine.cpp:119 soliddeviceengine.cpp:475
#: soliddeviceengine.cpp:481 soliddeviceengine.cpp:494
msgid "State"
msgstr "Status"

#: soliddeviceengine.cpp:120 soliddeviceengine.cpp:476
#: soliddeviceengine.cpp:482 soliddeviceengine.cpp:490
#: soliddeviceengine.cpp:492
msgid "Operation result"
msgstr "Handlingsresultat"

#: soliddeviceengine.cpp:121
msgid "Timestamp"
msgstr "Tidspunkt"

#: soliddeviceengine.cpp:129
msgid "Processor"
msgstr "Prosessor"

#: soliddeviceengine.cpp:130
msgid "Number"
msgstr "Nummer"

#: soliddeviceengine.cpp:131
msgid "Max Speed"
msgstr "Høgste fart"

#: soliddeviceengine.cpp:132
msgid "Can Change Frequency"
msgstr "Kan endra frekvens"

#: soliddeviceengine.cpp:140
msgid "Block"
msgstr "Blokk"

#: soliddeviceengine.cpp:141
msgid "Major"
msgstr "Hovud"

#: soliddeviceengine.cpp:142
msgid "Minor"
msgstr "Del"

#: soliddeviceengine.cpp:143
msgid "Device"
msgstr "Eining"

#: soliddeviceengine.cpp:151
msgid "Storage Access"
msgstr "Lagringstilgang"

#: soliddeviceengine.cpp:152 soliddeviceengine.cpp:506
#: soliddeviceengine.cpp:607
msgid "Accessible"
msgstr "Tilgjengeleg"

#: soliddeviceengine.cpp:153 soliddeviceengine.cpp:507
msgid "File Path"
msgstr "Filadresse"

#: soliddeviceengine.cpp:168
msgid "Storage Drive"
msgstr "Lagringsstasjon"

#: soliddeviceengine.cpp:171
msgid "Ide"
msgstr "IDE"

#: soliddeviceengine.cpp:171
msgid "Usb"
msgstr "USB"

#: soliddeviceengine.cpp:171
msgid "Ieee1394"
msgstr "IEEE 1394"

#: soliddeviceengine.cpp:172
msgid "Scsi"
msgstr "SCSI"

#: soliddeviceengine.cpp:172
msgid "Sata"
msgstr "SATA"

#: soliddeviceengine.cpp:172
msgid "Platform"
msgstr "Plattform"

#: soliddeviceengine.cpp:174
msgid "Hard Disk"
msgstr "Harddisk"

#: soliddeviceengine.cpp:174
msgid "Cdrom Drive"
msgstr "CD-ROM-spelar"

#: soliddeviceengine.cpp:174
msgid "Floppy"
msgstr "Diskett­stasjon"

#: soliddeviceengine.cpp:175
msgid "Tape"
msgstr "Kassett"

#: soliddeviceengine.cpp:175
msgid "Compact Flash"
msgstr "Compact Flash"

#: soliddeviceengine.cpp:175
msgid "Memory Stick"
msgstr "Minnepinne"

#: soliddeviceengine.cpp:176
msgid "Smart Media"
msgstr "Smart Media"

#: soliddeviceengine.cpp:176
msgid "SdMmc"
msgstr "SD MMC"

#: soliddeviceengine.cpp:176
msgid "Xd"
msgstr "XD"

#: soliddeviceengine.cpp:178
msgid "Bus"
msgstr "Buss"

#: soliddeviceengine.cpp:179
msgid "Drive Type"
msgstr "Stasjonstype"

#: soliddeviceengine.cpp:180 soliddeviceengine.cpp:193
#: soliddeviceengine.cpp:361 soliddeviceengine.cpp:375
msgid "Removable"
msgstr "Flyttbar"

#: soliddeviceengine.cpp:181 soliddeviceengine.cpp:194
#: soliddeviceengine.cpp:362 soliddeviceengine.cpp:376
msgid "Hotpluggable"
msgstr "Hotplug-kompatibel"

#: soliddeviceengine.cpp:203
msgid "Optical Drive"
msgstr "Optisk stasjon"

#: soliddeviceengine.cpp:208
msgid "CD-R"
msgstr "CD-R"

#: soliddeviceengine.cpp:211
msgid "CD-RW"
msgstr "CD-RW"

#: soliddeviceengine.cpp:214
msgid "DVD"
msgstr "DVD"

#: soliddeviceengine.cpp:217
msgid "DVD-R"
msgstr "DVD-R"

#: soliddeviceengine.cpp:220
msgid "DVD-RW"
msgstr "DVD-RW"

#: soliddeviceengine.cpp:223
msgid "DVD-RAM"
msgstr "DVD-RAM"

#: soliddeviceengine.cpp:226
msgid "DVD+R"
msgstr "DVD+R"

#: soliddeviceengine.cpp:229
msgid "DVD+RW"
msgstr "DVD+RW"

#: soliddeviceengine.cpp:232
msgid "DVD+DL"
msgstr "DVD+DL"

#: soliddeviceengine.cpp:235
msgid "DVD+DLRW"
msgstr "DVD+DLRW"

#: soliddeviceengine.cpp:238
msgid "BD"
msgstr "BD"

#: soliddeviceengine.cpp:241
msgid "BD-R"
msgstr "BD-R"

#: soliddeviceengine.cpp:244
msgid "BD-RE"
msgstr "BD-RE"

#: soliddeviceengine.cpp:247
msgid "HDDVD"
msgstr "HDDVD"

#: soliddeviceengine.cpp:250
msgid "HDDVD-R"
msgstr "HDDVD-R"

#: soliddeviceengine.cpp:253
msgid "HDDVD-RW"
msgstr "HDDVD-RW"

#: soliddeviceengine.cpp:255
msgid "Supported Media"
msgstr "Støtta medium"

#: soliddeviceengine.cpp:257
msgid "Read Speed"
msgstr "Lesefart"

#: soliddeviceengine.cpp:258
msgid "Write Speed"
msgstr "Skrivefart"

#: soliddeviceengine.cpp:266
msgid "Write Speeds"
msgstr "Skrivefartar"

#: soliddeviceengine.cpp:274
msgid "Storage Volume"
msgstr "Lagringsplass"

#: soliddeviceengine.cpp:277
#, kde-format
msgid "Other"
msgstr "Anna"

#: soliddeviceengine.cpp:277
#, kde-format
msgid "Unused"
msgstr "Ubrukt"

#: soliddeviceengine.cpp:277
#, kde-format
msgid "File System"
msgstr "Filsystem"

#: soliddeviceengine.cpp:277
#, kde-format
msgid "Partition Table"
msgstr "Partisjonstabell"

#: soliddeviceengine.cpp:277
#, kde-format
msgid "Raid"
msgstr "RAID"

#: soliddeviceengine.cpp:277
#, kde-format
msgid "Encrypted"
msgstr "Kryptert"

#: soliddeviceengine.cpp:280 soliddeviceengine.cpp:282
msgid "Usage"
msgstr "Bruk"

#: soliddeviceengine.cpp:282
#, kde-format
msgid "Unknown"
msgstr "Ukjend"

#: soliddeviceengine.cpp:285
msgid "Ignored"
msgstr "Ignorert"

#: soliddeviceengine.cpp:286
msgid "File System Type"
msgstr "Filsystemtype"

#: soliddeviceengine.cpp:287
msgid "Label"
msgstr "Merkelapp"

#: soliddeviceengine.cpp:288
msgid "UUID"
msgstr "UUID"

#: soliddeviceengine.cpp:297
msgid "Encrypted Container"
msgstr "Kryptert behaldar"

#: soliddeviceengine.cpp:309
msgid "OpticalDisc"
msgstr "Optisk plate"

#: soliddeviceengine.cpp:315
msgid "Audio"
msgstr "Lyd"

#: soliddeviceengine.cpp:318
msgid "Data"
msgstr "Data"

#: soliddeviceengine.cpp:321
msgid "Video CD"
msgstr "Video-CD"

#: soliddeviceengine.cpp:324
msgid "Super Video CD"
msgstr "Super Video-CD"

#: soliddeviceengine.cpp:327
msgid "Video DVD"
msgstr "Video-DVD"

#: soliddeviceengine.cpp:330
msgid "Video Blu Ray"
msgstr "Video Blu-ray"

#: soliddeviceengine.cpp:332
msgid "Available Content"
msgstr "Tilgjengeleg innhald"

#: soliddeviceengine.cpp:335
msgid "Unknown Disc Type"
msgstr "Ukjend platetype"

#: soliddeviceengine.cpp:335
msgid "CD Rom"
msgstr "CD-ROM"

#: soliddeviceengine.cpp:335
msgid "CD Recordable"
msgstr "CD-R"

#: soliddeviceengine.cpp:336
msgid "CD Rewritable"
msgstr "CD-RW"

#: soliddeviceengine.cpp:336
msgid "DVD Rom"
msgstr "DVD-ROM"

#: soliddeviceengine.cpp:336
msgid "DVD Ram"
msgstr "DVD-RAM"

#: soliddeviceengine.cpp:337
msgid "DVD Recordable"
msgstr "DVD-R"

#: soliddeviceengine.cpp:337
msgid "DVD Rewritable"
msgstr "DVD-RW"

#: soliddeviceengine.cpp:338
msgid "DVD Plus Recordable"
msgstr "DVD+R"

#: soliddeviceengine.cpp:338
msgid "DVD Plus Rewritable"
msgstr "DVD+RW"

#: soliddeviceengine.cpp:339
msgid "DVD Plus Recordable Duallayer"
msgstr "DVD+R DL"

#: soliddeviceengine.cpp:339
msgid "DVD Plus Rewritable Duallayer"
msgstr "DVD+RW DL"

#: soliddeviceengine.cpp:340
msgid "Blu Ray Rom"
msgstr "BD-ROM"

#: soliddeviceengine.cpp:340
msgid "Blu Ray Recordable"
msgstr "BD-R"

#: soliddeviceengine.cpp:341
msgid "Blu Ray Rewritable"
msgstr "BD-RE"

#: soliddeviceengine.cpp:341
msgid "HD DVD Rom"
msgstr "HD DVD-ROM"

#: soliddeviceengine.cpp:342
msgid "HD DVD Recordable"
msgstr "HD DVD-R"

#: soliddeviceengine.cpp:342
msgid "HD DVD Rewritable"
msgstr "HD DVD-RW"

#: soliddeviceengine.cpp:344
msgid "Disc Type"
msgstr "Platetype"

#: soliddeviceengine.cpp:345
msgid "Appendable"
msgstr "Tilleggbar"

#: soliddeviceengine.cpp:346
msgid "Blank"
msgstr "Tom"

#: soliddeviceengine.cpp:347
msgid "Rewritable"
msgstr "Overskrivbar"

#: soliddeviceengine.cpp:348
msgid "Capacity"
msgstr "Kapasitet"

#: soliddeviceengine.cpp:356
msgid "Camera"
msgstr "Kamera"

#: soliddeviceengine.cpp:358 soliddeviceengine.cpp:372
msgid "Supported Protocols"
msgstr "Støtta protokollar"

#: soliddeviceengine.cpp:359 soliddeviceengine.cpp:373
msgid "Supported Drivers"
msgstr "Støtta drivarar"

#: soliddeviceengine.cpp:370
msgid "Portable Media Player"
msgstr "Berbar mediespelar"

#: soliddeviceengine.cpp:384
msgid "Battery"
msgstr "Batteri"

#: soliddeviceengine.cpp:387
msgid "Unknown Battery"
msgstr "Ukjent batteri"

#: soliddeviceengine.cpp:387
msgid "PDA Battery"
msgstr "PDA-batteri"

#: soliddeviceengine.cpp:387
msgid "UPS Battery"
msgstr "UPS-batteri"

#: soliddeviceengine.cpp:388
msgid "Primary Battery"
msgstr "Primærbatteri"

#: soliddeviceengine.cpp:388
msgid "Mouse Battery"
msgstr "Musebatteri"

#: soliddeviceengine.cpp:389
msgid "Keyboard Battery"
msgstr "Tastaturbatteri"

#: soliddeviceengine.cpp:389
msgid "Keyboard Mouse Battery"
msgstr "Tastatur-/musebatteri"

#: soliddeviceengine.cpp:390
msgid "Camera Battery"
msgstr "Kamerabatteri"

#: soliddeviceengine.cpp:390
msgid "Phone Battery"
msgstr "Telefonbatteri"

#: soliddeviceengine.cpp:390
msgid "Monitor Battery"
msgstr "Skjermbatteri"

#: soliddeviceengine.cpp:391
msgid "Gaming Input Battery"
msgstr "Spelkontroll-batteri"

#: soliddeviceengine.cpp:391
msgid "Bluetooth Battery"
msgstr "Bluetooth-batteri"

#: soliddeviceengine.cpp:394
msgid "Not Charging"
msgstr "Ladar ikkje"

#: soliddeviceengine.cpp:394
msgid "Charging"
msgstr "Ladar"

#: soliddeviceengine.cpp:394
msgid "Discharging"
msgstr "Ladar ut"

#: soliddeviceengine.cpp:395
msgid "Fully Charged"
msgstr "Ferdig ladd"

#: soliddeviceengine.cpp:397
msgid "Plugged In"
msgstr "Kopla til"

#: soliddeviceengine.cpp:398
msgid "Type"
msgstr "Type"

#: soliddeviceengine.cpp:399
msgid "Charge Percent"
msgstr "Ladingsprosent"

#: soliddeviceengine.cpp:400
msgid "Rechargeable"
msgstr "Oppladbart"

#: soliddeviceengine.cpp:401
msgid "Charge State"
msgstr "Ladestatus"

#: soliddeviceengine.cpp:426
msgid "Type Description"
msgstr "Typeskildring"

#: soliddeviceengine.cpp:431
msgid "Device Types"
msgstr "Einingstypar"

#: soliddeviceengine.cpp:455 soliddeviceengine.cpp:554
msgid "Size"
msgstr "Storleik"

#: soliddeviceengine.cpp:534
#, kde-format
msgid "Filesystem is not responding"
msgstr "Filsystemet svarar ikkje"

#: soliddeviceengine.cpp:534
#, kde-format
msgid "Filesystem mounted at '%1' is not responding"
msgstr "Filsystemet montert på «%1» svarar ikkje"

#: soliddeviceengine.cpp:552
msgid "Free Space"
msgstr "Ledig plass"

#: soliddeviceengine.cpp:553
msgid "Free Space Text"
msgstr "Ledig plass-tekst"

#: soliddeviceengine.cpp:555
msgid "Size Text"
msgstr "Storleikstekst"

#: soliddeviceengine.cpp:581
msgid "Temperature"
msgstr "Temperatur"

#: soliddeviceengine.cpp:582
msgid "Temperature Unit"
msgstr "Temperatureining"

#: soliddeviceengine.cpp:626 soliddeviceengine.cpp:630
msgid "In Use"
msgstr "I bruk"
