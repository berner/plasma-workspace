# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-11 02:03+0000\n"
"PO-Revision-Date: 2014-07-04 09:44+0300\n"
"Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 1.5\n"

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Set flexible size"
msgstr "عيّن حجمًا مرنًا"
